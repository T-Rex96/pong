import pygame as pg

class Player:
    moveOffset = 8

    def __init__(self, pos, width, height, color, index):
        self.rect = pg.Rect(pos, (width, height))
        self.color = color
        self.index = index
        self.score = 0


    def draw(self, screen):
        pg.draw.rect(screen, self.color, self.rect)

    def move(self):
        keys = pg.key.get_pressed()

        downKey = pg.K_s if self.index == 0 else pg.K_DOWN
        upKey = pg.K_w if self.index == 0 else pg.K_UP
        if keys[downKey]:
            self.rect.y += self.moveOffset
        if keys[upKey]:
            self. rect.y -= self.moveOffset

