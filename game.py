import pygame as pg
from ball import Ball
from player import Player
from math import sin, cos, radians
from numpy import sign

# Some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

sizeX = 1000
sizeY = 700

playerXOffset = 30
playerWidth = 30
playerHeight = 150
player1Pos = (playerXOffset, sizeY / 2)
player2Pos = (sizeX - playerXOffset - playerWidth, sizeY / 2)

scoreYOffset = 40

# Set the height and width of the screen
size = (sizeX, sizeY)
screen = pg.display.set_mode(size)

class Game:
    def __init__(self):


        pg.display.set_caption("Pong")

        self.player1 = Player(player1Pos, playerWidth, playerHeight, WHITE, 0)
        self.player2 = Player(player2Pos, playerWidth, playerHeight, WHITE, 1)
        self.ball = Ball((sizeX / 2, sizeY / 2), WHITE)

    def start(self):
        clock = pg.time.Clock()
        done = False

        # Loop as long as done == False
        while not done:

            for event in pg.event.get():  # User did something
                if event.type == pg.QUIT:  # If user clicked close
                    done = True  # Flag that we are done so we exit this loop

            # All drawing code happens after the for loop and but
            # inside the main while not done loop.

            # Clear the screen and set the screen background
            screen.fill(BLACK)

            # Draw border
            pg.draw.rect(screen, WHITE, [0, 0, sizeX, sizeY], 5)

            for obj in (self.player1, self.player2, self.ball):
                obj.draw(screen)
                obj.move()

            self.checkBallCollision(self.ball)
            self.checkCollision(self.player1 if self.ball.vx<=0 else self.player2, self.ball)

            self.drawScore()

            # Go ahead and update the screen with what we've drawn.
            # This MUST happen after all the other drawing commands.
            pg.display.flip()

            # Limit to 60 fps
            clock.tick(120)

        pg.quit()

    def drawScore(self):
        font = pg.font.SysFont('Arial', 60, False, False)
        score1 = font.render(f"{self.player1.score}", True, WHITE)
        score2 = font.render(f"{self.player2.score}", True, WHITE)
        screen.blit(score1, score1.get_rect(center=(sizeX / 2 - 5 * playerWidth, scoreYOffset)))
        screen.blit(score2, score2.get_rect(center=(sizeX / 2 + 5 * playerWidth, scoreYOffset)))

    def checkBallCollision(self, ball):
        if ball.y <= ball.radius() or ball.y >= sizeY - ball.radius(): # Collision with top or bottom wall
            ball.vy = -ball.vy
        if ball.x <= 0 and self.ball.reset():
            self.player1.score += 1
        elif ball.x >= sizeX and self.ball.reset():
            self.player2.score += 1

    def checkCollision(self, player, ball):
        if ball.bottom() >= player.rect.top and ball.top() <= player.rect.bottom:
            collisionLeft = ball.vx <= 0 and ball.left() <= player.rect.right
            collisionRight = ball.vx >= 0 and ball.right() >= player.rect.left
            if collisionLeft or collisionRight:
                dist = player.rect.centery - ball.y
                angle = 2.0 * 55.0 / player.rect.height * dist

                sign = -1.0 if ball.vx >= 0.0 else 1.0
                ball.vx = sign * ball.vStart * cos(radians(angle))
                ball.vy = -ball.vStart * sin(radians(angle))
