#!/usr/bin/env python3
# Adapted from http://programarcadegames.com/python_examples/f.php?file=simple_graphics_demo.py 

import pygame as pg
from game import Game
 
# initialize pygame
pg.init()

game = Game()
game.start()

