import pygame as pg
from math import cos, sin, pi
import random

radius = 30

class Ball:
    def __init__(self, startingPos, color):
        self.startingPos = startingPos
        self.color = color
        (self.x, self.y) = startingPos
        self.vx = self.vy = 0.0

        self.vStart = 12.0

        self.reset()

    def reset(self):
        angle = random.uniform(0, 2.0 * pi)
        self.vx = self.vStart * cos(angle)
        self.vy = self.vStart * sin(angle)
        (self.x, self.y) = self.startingPos
        self.vStart *= 1.1
        return True

    # Watch out: The origin of the coordinate system in the TOP LEFT corner, so a positive y value means going DOWN!
    def left(self):
        return self.x - radius
    def right(self):
        return self.x + radius
    def bottom(self):
        return self.y + radius
    def top(self):
        return self.y - radius

    def radius(self):
        return radius

    def draw(self, screen):
        pg.draw.circle(screen, self.color, (int(self.x), int(self.y)), radius)

    def move(self):
        self.x += self.vx
        self.y += self.vy

